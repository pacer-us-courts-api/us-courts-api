import pprint
import CourtCodes

pp = pprint.PrettyPrinter()

fullname2cite = {}

for (name, id) in CourtCodes.NameToCode.items():
	for (id2, cite) in CourtCodes.courtIDtoCite.items():
		if id.startswith(id2):
			fullname2cite[name] = cite
			break
	
pp.pprint( fullname2cite)

raise Exception("done");

def foo():
	try:
		return
	except:
		print "except"
		raise
	finally:
		print "Finally"
foo()
print "returned"
raise Exception("done");


import pprint;
pp = pprint.PrettyPrinter(indent=4)

out = {}
for c in open("court_codes.txt").read().split("\n"):
	a = c.split("\t")
	try:
		if not a[1].strip():
			print "Can't do2 " + c;
			continue;
		out[a[1].strip()] = (a[0].strip(), a[2].strip(), a[3].strip());
	except:
		print "Can't do " + c;
pp.pprint(out);
	




raise Exception("done");

c = open("courts.txt").read();
types = ["U.S. Supreme Court",
			"National Locator",
			"U.S. Courts of Appeals", 
			"National Courts",
			"U.S. District Courts",
			"U.S. Bankruptcy Courts",
		]

def get_attr(s, start_txt, end_txt):
	alt_start = s.find(start_txt);
	if alt_start == -1:
		return "";
	alt_start += len(start_txt);
	
	alt_end = s.find(end_txt, alt_start);
	if alt_end == -1:
		return "";
	return s[alt_start:alt_end];

court_link_codes = {};
current_type = "";
for a in c.split("href="):
	for t in types:
		if a.find("<h4>%s</h4>"%t) != -1:
			current_type = t;
			break;

	end = a.find(".uscourts.gov");
	if end == -1:
		continue;
	end += len(".uscourts.gov");
	link = a[:end];

	name = get_attr(a[end:], "alt='", "'").replace("- ECF", "").strip();
	
	code = "?"
	code_start = link.find("ecf.");
	if code_start != -1:
		code = link[code_start + len("ecf."):link.find(".uscourts.gov")];
		court_link_codes[code] = link;
		
	#print "%s %s %s %s"%(current_type, link, code, name);
	
c = open("courts_search.txt").read();
court_codes = {};
for a in c.split("<select")[1:]:
	search_name = get_attr(a, 'name="', '"');
	this_court = {};
	for opt in a.split('<option '):

		opt = opt.replace("\n","");
		code = get_attr(opt, 'value="', '"');
		name = get_attr(opt, '>', '</').replace("&nbsp;","");
		#print "%s %s %s"%(search_name, code, name);
		link = court_link_codes.get(code);
		this_court[code] = name;
	
	court_codes[search_name] = this_court;

print pp.pformat(court_codes).replace("           ", "\t");