import pickle
import copy
import base64
import zlib
import time
import os, sys, pprint
import logging
logging.basicConfig(
	level = logging.DEBUG,
	format = '%(asctime)s %(levelname)s %(message)s',
)

ROOT_PATH = os.path.dirname(__file__)
sys.path.append(ROOT_PATH);
sys.path.append(os.path.join(ROOT_PATH, '../'));
import updater.LCS as LCS
	
from datetime import datetime;
import docket
import docket.pacer_parse as pacer_parse
from docket.pacer_parse import InitialParse
pp = pprint.PrettyPrinter(indent=4)

def get_soup(file, get_size=False):
	f = open(file)
	print "\nRunning Test on " + file;
	fred = f.read();
	size = len(fred)
	out = InitialParse(fred)
	f.close()
	return (out,size) if get_size else out

def print_cost(cost):
	if cost:
		print "%(time)s %(user)s $%(cost).2f %(contents)s"%(cost)
	
#soup = get_soup("out.htm");
#pp.pprint(pacer_parse.parse_cost(soup))
#pp.pprint(pacer_parse.parse_case_search_results(soup, "https://pcl.uscourts.gov/dquery"))

def print_timing(func):
    def wrapper(*arg):
        t1 = time.time()
        res = func(*arg)
        t2 = time.time()
        print '%s took %0.3f ms' % (func.func_name, (t2-t1)*1000.0)
        return res
    return wrapper()
	
files = [	{'f':"SearchResults.htm",'numresults':5,'numpages':0},
			{'f':"SearchResults2.htm",'numresults':54,'numpages':11},
			{'f':"SearchResults3.htm",'numresults':0,'numpages':0},
		# "SearchResults3.htm",  "SearchResults4.htm"
		]
for info in files:
	soup = get_soup(info['f']);
	results, pages = pacer_parse.parse_case_search_results(soup, "https://pcl.uscourts.gov/dquery")
	if info['numresults'] != len(results):
		raise Exception("Expecting %d results, got %d"%(info['numresults'], len(results)))
	if info['numpages'] != len(pages):
		raise Exception("Expecting %d pages, got %d"%(info['numpages'], len(pages)))
	if False:	
		pp.pprint(out);

soup = get_soup("QueryResultSample.htm");
out = pacer_parse.parse_pacer_query_result(soup, "https://ecf.ded.uscourts.gov/cgi-bin/iqquerymenu.pl?47007").links
if False:
	pp.pprint(out)

# I saved over this htm accidentallys
#soup = get_soup("DocketReportForm.htm");
#pp.pprint(pacer_parse.parse_run_docket_report_page(soup, "https://ecf.mdb.uscourts.gov/cgi-bin/DktRpt.pl?580392"));

def conversion_func(href, filing_number, exhibit_num):
	if exhibit_num:
		return "case/%s-%s"%(filing_number, exhibit_num)
	else:
		return "case/" + str(filing_number)

RunReportTests = True
if RunReportTests:
	files = [
			{'f':"DocketReport.htm", 'num_parties':3},
			"DocketReport2.htm", "DocketReport3.htm", 
			{'f':"DocketReport4.htm", 'num_parties':2, 'pnames':{1:'HARRIS N.A.'}},
			"DocketReport5.htm", 
			"DocketReport6.htm",
			{'f':"DocketReport7.htm", 'num_filings':79, 'num_parties':34, 'pnames':{0:'FastVDO LLC'}},
			{'f':"DocketReport8Big.htm", 'num_parties':8}, 
			{'f':"DocketReport9.htm",  'num_parties':0},
			{'f':"DocketReport10.htm", 'num_parties':0}, 
			{'f':"DocketReport11.htm", 'num_parties': 4, 'pnames': {2:'Alfred H Siegel (TR)'}},
			{'f':"DocketReport12.htm", 'num_parties': 6, 'pnames': {1:'Netflix, Inc.'}},
			{'f':"DocketReport13.htm", 'num_filings':101, 'num_parties':0},
			{'f':"DocketReport14.htm", 'num_filings':1, 'num_parties':8, 'pnames':{0:'Pankaj Topiwala', 7:'United States Trustee (LA)'}},
			{'f':"DocketReport15.htm", 'num_filings':10, 'num_parties':2 },
			]
	#files = [files[3]]
	for info in files:
		num_parties = None
		if isinstance(info, dict):
			num_parties = info['num_parties']
			f = info['f']
		else:
			f = info
			info = {} 
			
		soup, size = get_soup(f, True);
		docket_rep = pacer_parse.parse_docket_report(soup, "https://ecf.mdb.uscourts.gov/cgi-bin/DktRpt.pl?580392", conversion_func);
		other_rep = None#pacer_parse.parse_other_docket_items(soup)
		parties = pacer_parse.parse_docket_parties(soup)
		
		# pp.pprint(parties)
		# Make sure the number of parties are correct
		if num_parties != None and len(parties) != num_parties:
			raise Exception("Incorrect number of parties: %d"%len(parties))
		
		# Make sure the party names are correct
		if info.get('pnames'):
			for (k,v) in info['pnames'].items():
				if parties[k]['name'] != v:
					raise Exception("Expecting %s, got %s"%(v, parties[k]['name']))
		
		# Make sure there are the right number of filings
		num_filings = 0
		for x in docket_rep:
			if x.get('link'):
				num_filings += 1
		if info.get('num_filings') and info['num_filings'] != num_filings:
			raise Exception("Expecting %d filings, got %d"%(info['num_filings'], num_filings))
			
		# pp.pprint(other_rep)
		for x in docket_rep:
			contents = str(x['contents'][:50]) + "...";
			contents = contents.replace("\n"," ").replace("\r"," ")
			fix_link = lambda q: str(q).replace("http://","").replace("https://","").replace(".uscourts.gov","-").replace("cgi-bin","-")[:20]
			link = fix_link(x['link'])
			safe_link = fix_link(x['pacer_link'])
		# pp.pprint(docket_rep)
		if False and docket_rep:
			x = docket_rep[-1]	
			print "%s (%s): %s %s %s"%(str(x['number']), x['date'], link, safe_link, contents)
			print_cost(pacer_parse.parse_cost(soup));
		P = pickle.dumps(docket_rep, 1)
		ZP =  print_timing(lambda : zlib.compress(P, 3))
		B64P = base64.b64encode(P)
		B64ZP = base64.b64encode(ZP)
		reduce = 100*len(B64P)/len(B64ZP)
		# print "Total Size: %dkb, %.1f%% Reduction, Original Size: %dkb (Just Pickeled: %dkb)"%(len(B64ZP)/1024, reduce, size/1024, len(P)/1024)


soup = get_soup("DocketReport7.htm");
docket_rep = pacer_parse.parse_docket_report(soup, "https://ecf.mdb.uscourts.gov/cgi-bin/DktRpt.pl?580392", conversion_func)
C = LCS.LCS(docket_rep[1:-1], docket_rep[:])
if len(LCS.getNew(docket_rep[1:-1], docket_rep[:])) != 2:
	raise Exception("Expecting two differences")
if LCS.getNew(docket_rep[:], docket_rep[:]) != []:
	raise Exception("Expecting no differences")

other_items = pacer_parse.parse_other_docket_items(soup)
other_items2 = copy.copy(other_items)
del other_items2['judge']
if LCS.getNew(other_items, other_items2):
	raise Exception("Expecting no changes")
if not LCS.getNew(other_items2, other_items).get('judge'):
	raise exception("Expecting change in judge")

soup = get_soup("NotLoggedIn.html");
if pacer_parse.parse_loginsuccess(soup):
	raise Exception("Did not detect login page")

# Download Form Tests
download_tests = [
	{'f': "DownloadForm.html", 'ref':"", 'chk': lambda x: x},
	{'f': "DownloadForm2.html", 'ref':"https://ecf.cacd.uscourts.gov/doc1", 'chk': "https://ecf.cacd.uscourts.gov/doc1/031113584824"},
	{'f': "DownloadForm3.html", 'ref':"https://ecf.cacd.uscourts.gov/doc1", 'chk': "https://ecf.cacb.uscourts.gov/doc1/973153305132"},
	{'f': "DownloadForm4.html", 'ref':"https://ecf.cacd.uscourts.gov/cgi-bin/", 'chk': "https://ecf.cacd.uscourts.gov/doc1/031113694142"},
	]
for d in download_tests:
	soup = get_soup(d['f']);
	the_link = pacer_parse.parse_try_document_select_parse(soup, "")
	if isinstance(d['chk'], str):
		if the_link != d['chk']:
			raise Exception("Did not find download link.\nExpecting %s, got %s."%(d['chk'], the_link))
	elif not d['chk'](the_link):
		raise Exception("Did not find download link. Check Failed.")

	
# This one is an order form
soup = get_soup("DownloadOrderForm.html");
download_link, data = pacer_parse.parse_download_order_form(soup, "https://ecf.cacb.uscourts.gov/doc1/973051858591")
if download_link != "https://ecf.cacb.uscourts.gov/doc1/973151858591":
	raise Exception("Did not find download link.")
