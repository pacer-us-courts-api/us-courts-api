'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from datetime import datetime
import BeautifulSoup
from urlparse import urljoin
import logging
import types
import re
import copy
import pprint

	
class Returnable:
	pass
isNavigable = lambda s: isinstance(s, BeautifulSoup.NavigableString)

IsSoup = lambda s: isNavigable(s) or isinstance(s, BeautifulSoup.Tag)

def InitialParse(data):
	data = data.replace("&nbsp;", " ")
	masssage_bad_comments = [(re.compile('<!-([^-])'), lambda match: '<!--' + match.group(1))]
	myNewMassage = copy.copy(BeautifulSoup.BeautifulSoup.MARKUP_MASSAGE)
	myNewMassage.extend(masssage_bad_comments)
	return BeautifulSoup.BeautifulSoup(data, markupMassage=myNewMassage)

def warn(w):
	logging.warning(w)
	raise Exception(w)
	#print "WARN: "+ w

pp = None
def debug(d):
	global pp
	if not pp:
		pp = pprint.PrettyPrinter(indent=4)
	pp.pprint(d)
	#logging.info(d)
	
def _parse_cookie_set(soup):
	allTexts = soup.findAll("script", text=re.compile("document.cookie"))
	
	out = {}
	for text in allTexts:
		if not text:
			continue
		text = text.replace("document.cookie", "").strip()
		if text[0] != '=':
			continue
		text = text[1:].strip()
		if text[-1] == "":
			text = text[:-1].strip()
		if text[0] == "'" and text[-1] == "'":
			text = text[1:-1].strip()
		elif text[0] == '"' and text[-1] == '"':
			text = text[1:-1].strip()
		else:
			continue
		text = text.split("=")
		out[text[0].strip()] = text[1].strip()
		
	return out

def __to_soup(t, soup, tofunc):
	if not soup:
		return soup
	soup = tofunc(soup)
	while soup and ("NavigableString" in str(type(soup))  or soup.name != t):
		soup = tofunc(soup)
	return soup

def _to_parent(t, soup):
	return __to_soup(t, soup, lambda s: s.parent)

def _to_prev(t, soup):
	return __to_soup(t, soup, lambda s: s.previousSibling)

def _to_next(t, soup):
	return __to_soup(t, soup, lambda s: s.nextSibling)
	
def parse_cost(soup):
	table = soup.find("table", id="receipt")
	if table:		
		'''
		Parse a Cost Table Like This
		<table id="receipt" class="center">
		  <tr><th align="left" nowrap colspan=2>&nbsp</th><td align="right" colspan=2 nowrap><b>Receipt</b> 10/30/2011 19:59:13 75984064</td></tr>
		  <tr><th align="right" width="10%" class="shade">&nbspUser</th><td>pacerlogin</th></tr>
		  <tr><th align="right" width="10%" class="shade">&nbspClient</th><td colspan=2></th></tr>
		  <tr><th align="right" class="shade">&nbspDescription</th><td colspan=2>All Court Types Case Search </td></tr>
		  <tr><th>&nbsp</th><td colspan=2>2011-00797 Delaware  Page: 1</td></tr>
		  
		  <tr><th align="right" width="10%" class="shade">&nbspPages</th><td>1 ($0.08)</td></tr>
		</table>
		'''
		entries = table.findAll("td")
		out = {
			'receipt': unicode(entries[0].contents[1]),
			'user': unicode(entries[1].contents[0]),
			'contents': unicode(entries[4].contents[0]),
			'cost': unicode(entries[5].contents[0]),
		}
		split_receipt = out['receipt'].strip().split(" ")
		date_and_time = " ".join(split_receipt[0:2])
		
		out['receipt'] = split_receipt[2]
		out['time'] = datetime.strptime(date_and_time, "%m/%d/%Y %H:%M:%S")
		
		# Get the cost value
		start_cost = out['cost'].find("$")
		if table.find(text=re.compile("You have previously been billed")):
			out['cost'] = 0.0
		elif start_cost != -1:
			end_cost = out['cost'].find(")", start_cost)
			if end_cost != -1:
				out['cost'] = float(out['cost'][start_cost+1:end_cost])
			else:
				raise Exception("Unknown Cost Value: %s"%(out['cost']))
		else:
			raise Exception("Unknown Cost Value: %s"%(out['cost']))
			out['cost'] = 0.0
		return out
	
	# Get the last table on the page
	table = soup("table")
	if table and len(table) >= 1:
		table = table[-1]
	if table and _to_parent("th", table.find(text=re.compile("PACER Login"))):
		'''
		<TABLE BORDER=1 BGCOLOR=white width="400">
		<TR><TH COLSPAN=4><FONT SIZE=+1 COLOR=DARKRED>PACER Service Center </FONT></TH></TR>
		<TR><TH COLSPAN=4><FONT COLOR=DARKBLUE>Transaction Receipt </FONT></TH></TR>
		<TR></TR>
		<TR></TR>
		<TR><TD COLSPAN=4 ALIGN=CENTER><FONT SIZE=-1 COLOR=DARKBLUE>11/05/2011 17:44:28</FONT></TD></TR>
		<TR><TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> PACER Login: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> PACERloginid </FONT></TH></TD>
			<TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Client Code: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE>  </FONT></TD></TR>
		<TR><TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Description: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Docket Report </FONT></TD>
			<TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Search Criteria: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> 11-00797 Fil or Ent: filed From: 1/1/1990 To: 11/05/2011 Doc From: 0 Doc To: 99999999 Term: included    Format: html </FONT></TD></TR>
		<TR><TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Billable Pages: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> 2 </FONT></TD>
			<TH ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> Cost: </FONT></TH>
			<TD ALIGN=LEFT><FONT SIZE=-1 COLOR=DARKBLUE> 0.16 </FONT></TD></TR>
		<TR></TR><TR></TR>
		</TABLE>
		'''
		#print _to_parent("th", table.find(text=re.compile("Cost")))
		#print table.find("th",text=re.compile("PACER Login")).nextSibling
		#print _to_next("td", _to_parent("th", table.find("th", text=re.compile("PACER Login")))).find(text=True).strip()
		out = {
			'receipt': "",
			'user': _to_next("td", _to_parent("th", table.find(text=re.compile("PACER Login")))).find(text=True).strip(),
			'contents': _to_next("td", _to_parent("th", table.find(text=re.compile("Description")))).find(text=True).strip(),
			'cost': _to_next("td", _to_parent("th", table.find(text=re.compile("Cost")))).find(text=True).strip(),
		}
		#print _to_prev("tr", _to_parent("tr", table.find(text=re.compile("PACER Login")))).find(text=True).strip()
		date_and_time = _to_prev("tr", _to_parent("tr", table.find(text=re.compile("PACER Login")))).find(text=True).strip()
		out['time'] = datetime.strptime(date_and_time, "%m/%d/%Y %H:%M:%S")
		
		out['cost']  = float(out['cost'].replace("$","").replace("(","").replace(")","").strip())

		return out
	
	return {}

def parse_loginsuccess(soup):
	'''
	Return False if we are still on a login screen.
	Return true if no login screen is returned. (we are not necessarily logged in though).
	'''
	if soup.find(text=re.compile("Login Error")):
		return False
	if soup.findAll("input", id="loginid"):
		return False
	if soup.findAll("input", attrs={"name" : "login"}):
		return False
	# print soup
	return True
	
def _fix_title(title):
	import BlueBook
	#return BlueBook.abbreviate_title(title) # eh... don't like this
	return title

def parse_case_search_results(soup, base_url):
	from court_codes import CourtCodes
	'''
	<tr><td class="line_no">1</td> 
		<td class="cs_title">FastVDO LLC v. Apple Inc. et al</td> 
		<td class="court_id" >dedce</td> 
		<td class="case"><a href='https://ecf.ded.uscourts.gov/cgi-bin/iqquerymenu.pl?47007' title='FastVDO LLC v. Apple Inc. et al'>1:2011-cv-00797</a></td> 
		<td class="nos">830</td> 
		<td class="cs_date">09/09/2011</td> 
		<td class="cs_date"></td> 
	</tr>
	
	Also look for pagination. This is a summary of that
	<div id="page_select" class="center"><a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=1"><span class="glink">Previous</span></a>&nbsp;<a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=1" onClick="cursor_wait();"><span class="plink">1</span></a> <span id='complete' class="slink">2</span> <a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=3" onClick="cursor_wait();"><span class="plink">3</span></a> <a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=4" onClick="cursor_wait();"><span class="plink">4</span></a> <a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=5" onClick="cursor_wait();"><span class="plink">5</span></a> &nbsp;<a href="/view?rid=9lXdcGa6gq2eMaoN5TqjvG3VFYiforY4H3V9JvN4&page=3" onClick="cursor_wait();"><span class="glink">Next</span></a></div>
	'''
	if not parse_loginsuccess(soup):
		raise Exception("Not Logged In to PACER, Cannot Parse Search Results")

	if soup.find("span", "error"):
		raise Exception(soup.find("span", "error").find(text=True))
	search_results = []
	result_set = map(lambda p: p.parent, soup.findAll("td", {'class' : 'cs_title'} ))
	if not result_set:
		result_set = map(lambda p: p.parent, soup.findAll("td", {'class' : 'court_id'} ))
	
	for p in result_set:
		case = {
			'court_id':p.find("td", {'class' : 'court_id'}).contents[0],
			'cs_date' : p.find("td", {'class' : 'cs_date'}).contents[0],
			'link': urljoin(base_url, p.find("td", {'class' : 'case'}).a['href']),
			'docket':p.find("td", {'class' : 'case'}).a.contents[0],
		}
		nos = p.find("td", {'class' : 'nos'})
		if nos and nos.contents:
			case['nos'] = nos.contents[0]
				# Get the Title
		
		title = p.find("td", {'class' : 'cs_title'})
		if title:
			case['title'] = title.contents[0]
		else:
			case['title'] = p.find("td", {'class' : 'case'}).a['title']
		case['title'] = _fix_title(case['title'])
		
		case['court_name'] = CourtCodes[case['court_id']][0]
		
		search_results.append(case)
	# Remove the duplicates by building a dictionary
	search_dict = {}
	for s in search_results:
		search_dict[s['docket']] = s
	search_results = search_dict.values()
	
	# Now look for pagination
	pages = {};
	if soup.find(id="page_select"):
		for a in soup.find(id="page_select").findAll("a"):
			if a.find(text=re.compile("Previous")) or a.find(text=re.compile("Next")):
				continue;
			try:
				key = int(a.span.string)
			except ValueError:
				continue;
			pages[key] = urljoin(base_url, a['href'])
	
	return search_results, pages

def parse_pacer_query_result(soup, base_url):
	
	'''
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/qryAlias.pl?580392">Alias</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/qryAscCases.pl?580392">Associated Cases</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/qryAttorneys.pl?580392">Attorney</a><br>

	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/qrySummary.pl?580392">Case Summary</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/SchedQry.pl?580392">Deadline/Schedule</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/DktRpt.pl?580392">Docket Report ...</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/FilerQry.pl?580392">Filers</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/HistDocQry.pl?580392">History/Documents</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/qryParties.pl?580392">Party</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/RelTransactQry.pl?580392">Related Transactions</a><br>
	&nbsp&nbsp&nbsp<a href="https://ecf.mdb.uscourts.gov/cgi-bin/StatusQry.pl?580392">Status</a><br>
	'''
	if not parse_loginsuccess(soup):
		raise Exception("Not Logged In to PACER, Cannot Parse Search Results")
	
	look_for = ['Alias', 	'Associated Cases', 
				'Attorney', 'Case Summary', 
				'Deadline', 'Docket Report', 
				'Filer', 	'History', 
				'Party', 	'Related Transactions', 
				'Status']
	links = {}
	for l in look_for:
		text = soup.find(text=re.compile(l))
		if text:
			a = text.parent
			if a.name != "a":
				continue
			links[l] = urljoin(base_url, a['href'])
	
	out = Returnable()
	out.links = links
	out.cookies = _parse_cookie_set(soup)
	
	return out

def parse_run_docket_report_page(soup, base_url):
	'''
	<FORM ENCTYPE='multipart/form-data' method=POST action="/cgi-bin/DktRpt.pl?259663742382027-L_1_0-1" >
	'''
	if not parse_loginsuccess(soup):
		raise Exception("Not Logged In to PACER, Cannot Parse the Run Docket Report Page")

	form = soup.find("form", method="POST")
	if not form:
		form = soup.find("form", method="post")
	
	form_data = {}
	form_vals = ["all_case_ids", "date_from", "date_to", "date_type"]
	
	for fval in form_vals:
		form_item = form.find("input", {'name' : fval})
		if form_item and form_item.get('value'):
			form_data[fval] = form_item['value']
	
	return (urljoin(base_url, form['action']), form_data)

	
def parse_docket_report(soup, base_url, link_safe_func):
	'''
	Return a list of docket entries, dictionaries, with the following elements
		date 		Date of the docket entry
		pacer_link	A link to download the docketed item on PACER, if it exists
		link		A link that has been processed through link_safe_func
		conents		The text associated with the summary of the docket entry
		number		The docket number, if it exists
		exhibits	A list of attached exhibits, dictionaries, with the following entries
						exhibit		the exhibit name or number
						pacer_link 	a link to the exhibit on pacer
						link		an internal link to the exhibit
	link_safe_func: function taking returning a safe link given the following args
					href, filing_number, anchor, is_exhibit
	'''
	if not parse_loginsuccess(soup):
		raise Exception("Not Logged In to PACER, Cannot Parse the Docket Report")
	
	text = soup.find("th", text=re.compile("Docket Text"))
	if not text:
		logging.error("No Docket Table Found")
		return []
	
	out = []
	table = text.parent
	while str(table.name) != "table":
		table = table.parent
	
	
	last_num = 0
	# We will need this map later to determine if a link is an exhibit
	for row in table.findAll("tr"):
		cols = row.findAll("td")
		
		if len(cols) < 3:
			continue
		if len(cols) == 3:
			(date_col, link_num_col, contents_col) = (0, 1, 2)
		elif len(cols) in [4, 5]:
			(date_col, link_num_col, contents_col) = (0, 2, 3)
					
		r = { }
		# Get the date, it's relatively simple
		r['date'] = " ".join(map(lambda x: str(x), cols[date_col].contents));
		r['date'] = datetime.strptime(r['date'], "%m/%d/%Y")
		
		has_link = cols[link_num_col].a != None
		
		# Get the link to the PACER entry for this filing number
		r['pacer_link']		= urljoin(base_url, cols[link_num_col].a.get("href")) if has_link else None
		# Get the filing number
		num_str = cols[link_num_col].a.contents[0] if has_link else cols[link_num_col].contents[0]
		num_str = str(num_str).strip()
		try:
			r['number'] = int(num_str)
		except ValueError:
			if num_str:
				warn("Could not parse filing number: %d %s"%(link_num_col, str(cols[link_num_col])))
			r['number'] = None
			
		# Get the internal version of the link
		r['link'] = link_safe_func(href = r['pacer_link'], 
				filing_number = r['number'], exhibit_num = None
				) if link_safe_func and r['pacer_link'] else None
		r['exhibits'] = []
		
		# Convert all the links in the contents and get the exhibits
		r['exhibits'] = []
		contents_soup = cols[contents_col]
		found_attachments = False
		for link in contents_soup.findAll("a"):
			anchor = " ".join(map(lambda x: unicode(x), link.contents))
			if not found_attachments and link.previousSibling and (
					"Attachments:" in unicode(link.previousSibling) or
					"(Additional attachment(s) added" in unicode(link.previousSibling)):
				found_attachments = True
			
			is_exhibit = found_attachments
			# Figure out the safe link
			if not link_safe_func:
				internal_link = None
			elif not is_exhibit:
				# The link is referring to a different filing
				internal_link = link_safe_func(link['href'], anchor, exhibit_num=None)
			else:
				internal_link = link_safe_func(link['href'], r['number'], exhibit_num=anchor)
			# Add the exhibit if this is one
			if is_exhibit:
				r['exhibits'].append({
					'exhibit':anchor,
					'pacer_link' : link['href'],
					'link' : internal_link,
				})
			# Change the html so it points to the safe link
			link['href'] = internal_link
			for (k,v) in link.attrs:
				if k != 'href':
					del link[k]
			link['filing'] = r['number']
			if is_exhibit:
				link['exhibit'] = anchor	
			
		r['contents'] = " ".join(map(lambda x: unicode(x), contents_soup));
		
		out.append(r)
		
	return out

def parse_other_docket_items(soup):
	'''
	'''
	out = {}
	# Only interested in the "main stuff"
	soup = soup.find(id="cmecfMainContent")
	
	def get_txt_after(the_text):
		def remove_str_after_white(in_str):
			after_white = in_str.find("   ")
			return (in_str if after_white == -1 else
				in_str[:after_white]).strip()
				
		s = soup.find(text=re.compile(the_text))
		if not s:
			return None
		after_text = str(s).strip()
		after_text_f = str(after_text).find(the_text)
		after_text = after_text[after_text_f + len(the_text):]
		if after_text:
			return remove_str_after_white(after_text)
		if not s.next:
			return None
		if not isNavigable(s.next):
			return None
		return str(s.next).strip()
		
	
	# Get the nature of suit
	# TODO: This doesn't work for certain weird dockets DocketReport4.htm
	out['nos'] = get_txt_after("Nature of Suit:")
			
	# Get the judges
	out['judge'] = get_txt_after("Assigned to:")
	out['magistrate'] = get_txt_after("Referred to:")
	
	# Other easy info
	out['demand'] = get_txt_after("Demand:")
	out['cause'] = get_txt_after("Cause:")
	out['date_filed'] = get_txt_after("Date Filed:")
	out['date_terminated'] = get_txt_after("Date Terminated:")
	
	return out

def get_parent_row(s):
	while s and (isNavigable(s) or s.name != 'tr'):
		s = s.parent
	return s if s else None
def get_previousSiblingTag(s):
	s = s.previousSibling
	while s and isNavigable(s):
		s = s.previousSibling
	return s
def get_nextSiblingTag(s):
	s = s.nextSibling
	while s and isNavigable(s):
		s = s.nextSibling
	return s
		
def parse_docket_parties(soup):
	out = []
	if soup.find("pre"):
		return out
	party_line_marker = "-----------------------"
	party_types = ["Plaintiff", "Defendant", "Counter Claimant", "Counter Defendant",
					"Debtor", "Trustee", "U.S. Trustee", "Joint Debtor"]
	party_types = map(lambda r: r"^%s[\s]*$"%r, party_types)
	for row in soup.findAll(text=re.compile("|".join(party_types))):
		party = {}
		row = get_parent_row(row)
		if not row:
			warn("Row directly before 'represented by' should contain party type")
			continue

		##############################
		# Figure out if the party is a plaintiff, defendant, whatever
		# Find our bearings. Some dockets use underline, others use ---------...
		line_marker = row.find(text=re.compile(party_line_marker))
		if line_marker:
			# we found the marker, the party type is right above
			row = line_marker.previous
			while row and not isNavigable(row):
				row = row.previous
		else:
			row = row.find("u") or row.find("i")
			if not row:
				warn("Could not find italic, underline, or marker")
				continue
			row = row.find(text=True)
		if not row:
			warn("Could not find party type")
			continue
		party['type'] = str(row).strip()
		
		###############################
		# Get the row that contains the party's name
		row = get_parent_row(row)
		if not row:
			warn("Malformed html!")
			continue
		if(len(row.findAll("td")) == 1):
			# Convert the cell into text
			check_party_row = map(lambda x: str(x).strip(), row.findAll(text=True))
			# Remove the party type, the line-marker, and whitespace
			check_party_row = filter(lambda x: x and x!= party['type'] and party_line_marker not in x, check_party_row)
			if not check_party_row:
				# The party name is in the row directly below us
				row = get_nextSiblingTag(row)
				if not row:
					warn("Could not find party name row")
					continue
		
		# The party's name will be in the first cell of the row
		row = row.findAll("td")
		party_name = row[0]
		if not party_name:
			warn("Could not find party name field")
			continue
		# Convert the cell into text
		party_name = map(lambda x: unicode(x).strip(), party_name.findAll(text=True))
		# Remove the party type, the line-marker, and whitespace
		party_name = filter(lambda x: x and x!= party['type'] and party_line_marker not in x, party_name)
		# If anything is left, the name should be the first item
		if party_name:
			party['name'] = unicode(party_name[0])
		else:
			logging.warning("Cannot find party name")
		
		###############################
		# Get the party's counsel
		party['counsel'] = _parse_counsel_td(row[2]) if len(row) > 1 else []
		out.append(party)
		
	# debug( out)
		
	return out

def _parse_counsel_td(soup):
	'''
	Internal function, parses information within the counsel's 'td'
	'''
	def removeSpaceBetween(w):
		return " ".join(filter(lambda x: x.strip(), w.split(" ")))

	def nSplit(lst, is_delim, count=2):
		'''
		Split a list every time 'coount' number of delimiters are found
		'''
		output = [[]]
		delimCount = 0
		for item in lst:
			if is_delim(item):
				delimCount += 1
			elif delimCount >= count:
				output.append([item])
				delimCount = 0
			else:
				output[-1].append(item)
				delimCount = 0
		return output
	
	lines = soup.contents
	# Remove empty (i.e. whitespace only) lines
	lines = filter(lambda x: x.strip() if isNavigable(x) else x, lines)
	# Sometimes there is a font tag we need to unwrap
	if(len(lines) == 1 and
		not isNavigable(lines[0]) and
		lines[0].name == 'font'):
		lines = lines[0].contents
	
	counsels_list = nSplit(lines, lambda x: not isNavigable(x) and x.name == 'br')
	# Sometimes there are extra line breaks, remove them
	counsels_list = filter(lambda x: x, counsels_list)
	
	counsels = []
	for c in counsels_list:
		# Break the counsel name up line-by-line
		c = map(lambda x: " ".join(x.findAll(text=True)) if not isNavigable(x) else x, c)
		# Remove extra whitespace
		c = map(lambda x: removeSpaceBetween(x.strip()).strip(), c)
		# Get rid of empty entries
		c = filter(lambda x: x.strip() if isNavigable(x) else x, c)
		
		if not c:
			continue
		
		counsel = {
			'name' : removeSpaceBetween(c[0]),
		}
		tags = {
			'Email:' : 'email',
			'Fax:' : 'fax',
			'Fax :' : 'fax',
		}
		c = c[1:]
		for i, l in enumerate(c):
			for k,v in tags.items():				
				if l.startswith(k):
					tag = l[len(k):].strip()
					if tag:
						counsel[v] = tag
					c[i] = ""
		attrs = {
			'lead attorney' : 'lead',
			'attorney to be noticed' : 'noticed',
			'pro hac vice' : 'pro hac vice',
			'pro se' : 'pro se',
		}
		for i, l in enumerate(c):
			for k,v in attrs.items():
				counsel[v] = (l.lower().strip() == k)
				if counsel[v]:
					c[i] = ""
		c = filter(lambda x: x.strip(), c)
		counsel['other'] = c
		counsels.append(counsel)
		
	return counsels

def parse_try_document_select_parse(in_soup, base_url):
	'''
	If a document has attachements, we may need to select the document directly before "ordering" it
	<td colspan=2><B>Document Number:</B> <a href="https://ecf.cacd.uscourts.gov/doc1/031113584921" onClick="goDLS('/doc1/031113584921','518801','12','','1','0','','');return(false);">1</a></td>
	'''
	soup = in_soup.find(text=re.compile(r"^Document Number:?$"))
	if soup:
		while soup and (isNavigable(soup) or soup.name != "td"):
			# Find our parent td
			soup = soup.parent
		if not soup:
			raise Exception("Expected to find the parent cell.")
		soup = soup.find("a")
		if not soup:
			raise Exception("Expected to find document link")
		return urljoin(base_url, soup['href'])
	soup = in_soup.find(text=re.compile(r"^Part$"))
	if soup:
		soup = get_parent_row(soup)
		if not soup:
			raise Exception("Expected to find the parent row.")
		soup = get_nextSiblingTag(soup)
		if not soup or isNavigable(soup) or soup.name != "tr":
			raise Exception("Expected to find the next row (%s)."%str(soup))
		soup = soup.find("a")
		if not soup:
			raise Exception("Expected to find the order link.")
		if not soup.find(text=re.compile("1")):
			raise Exception("Expected to find the link '1'.")
		return urljoin(base_url, soup['href'])
	return None
	
def parse_download_order_form(soup, base_url):
	'''
	<form method=post action='https://ecf.ded.uscourts.gov/doc1/04311485151'   onSubmit="goDLS('/doc1/04311485151','47007',  '44','1','','1','','');return(false);">
	and also
	<FORM method=POST ACTION='https://ecf.cacb.uscourts.gov/doc1/973151858591' onSubmit="goDLS('/doc1/973151858591','1382179','', '1','','', '','','','');return(false)">
	'''
	# function goDLS(hyperlink, de_caseid, de_seqno, got_receipt, pdf_header, pdf_toggle_possible, magic_num, hdr) {
	# we need: caseid, de_seq_num, got_receipt, pdf_toggle_possible
	if not parse_loginsuccess(soup):
		raise Exception("Not Logged In to PACER, Cannot Parse the Docket Report")
	if soup.find(text=re.compile("not have permission to view")):
		raise Exception("This document has been sealed and is not publicly available.")
	if soup.find(text=re.compile(r"The document you are attempting to access \([0-9]+\) cannot be found")):
		raise Exception("This document is listed on PACER but is unavailable, it may be under seal.")
	if soup.find(text=re.compile(r"This document is not available.")):
		raise Exception("This document is listed on PACER but is unavailable, it may be under seal.")
	form = soup.find("form")
	if not form:
		raise Exception("No form submission button to download document.")
	
	onsubmit = form['onsubmit']
	onsubmit = onsubmit.replace("goDLS(","")
	onsubmit = onsubmit.replace(");return(false);", "").replace(");return(false)", "")
	onsubmit = onsubmit.split(",")
	onsubmit = map(lambda x: x[1:-1], onsubmit)
	# The data keys we need to submit
	keys = ['', 'caseid', 'de_seq_num', 'got_receipt', '', 'pdf_toggle_possible', '', '', '', '']
	data = {}
	for i, val in enumerate(onsubmit):
		if val and i < len(keys) and keys[i]:
			data[keys[i]] = val
	
	download_link = form['action']
	
	return download_link, data

def parse_downloaded_frame(soup, base_url):
	'''
	<iframe src="/cgi-bin/show_temp.pl?file=1297196-0--27989.pdf&type=application/pdf" height="100%" width="100%" frameborder="0" scrolling="no">
	'''
	iframe = soup.find("iframe")
	return urljoin(base_url, iframe['src']) if iframe else None

	