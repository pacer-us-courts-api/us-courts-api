'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import os, sys, urllib, urllib2, time, cookielib, logging, copy, pprint, urlparse
import pickle
import base64

if __name__ == "__main__":
	logging.basicConfig(
		level = logging.INFO,
		format = '%(asctime)s %(levelname)s %(message)s',
	)
	ROOT_PATH = os.path.dirname(__file__)
	sys.path.append(ROOT_PATH)
	sys.path.append(os.path.join(ROOT_PATH, '../'))

import Cookie
from poster.encode import multipart_encode
import poster.streaminghttp
from datetime import datetime
import pacer_parse
from pacer_parse import InitialParse, IsSoup
from gaeurllib import URLOpener

TESTING_USE_LOCAL = False

urlopen = urllib2.urlopen
DEBUG = False
VERBOSE_DEBUG = False
default_headers = {
	"User-Agent" : "Mozilla/5.0 (X11 U Linux i686) Gecko/20071127 Firefox/2.0.0.11",
	"Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	"Accept-Language" : "en-us,en;q=0.5",
	"Accept-Charset" : "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
	
	# FIXME: Is this necessary? should I use no-store, which is even stronger?
	"Pragma":"no-cache", 
	"Cache-Control":"no-cache",
}

# If we see a login prompt on this page, we're not logged in
login_check_page = "https://pcl.uscourts.gov/search"
login_check_page = "https://pacer.login.uscourts.gov/cgi-bin/login.pl"

login_post_page = 'https://pacer.login.uscourts.gov/cgi-bin/check-pacer-passwd.pl'
login_post_data = {
		'loginid':'',	# PACER login id
		 'passwd':'',	# PACER password
		 'client':'',	# PACER client matter number
		 'faction':'Login',
		 'appurl':'',
		 'court_id':'',
		 }

		 
case_types = {
	"Civil" : "cv",
	"Criminal" : "cr",
	"Bankruptcy" : "bk",
	"Appellate" : "ap",
}

search_post_url = "https://pcl.uscourts.gov/dquery"
search_post_data_default = {
	"all_region": "00",
	"ap_region": "00",
	"bk_region": "00",
	"dc_region": "00",
	"case_no": "",
	"mdl_id": "",
	"stitle": "",
	"chapter": "",
	"date_filed_start": "",
	"date_filed_end": "",
	"date_term_start": "",
	"date_term_end": "",
	"date_dismiss_start": "",
	"date_dismiss_end": "",
	"date_discharge_start": "",
	"date_discharge_end": "",
	"party": "",
	"ssn4": "",
	"ssn": "",
	"court_type": "all",
}

docket_report_defaults = {
	"view_comb_doc_text" : "",
	"all_case_ids" : "",		# This may be important, I'm not sure what it is
	# "CaseNum_47007"  : "on",	# diddo
	"date_range_type" : "Filed",
	"date_type" : "filed",
	"date_from" : "1/1/1990",
	"date_to" : datetime.utcnow().strftime("%m/%d/%Y"),
	"documents_numbered_from_" : "\n",
	"documents_numbered_to_" : "\n",
	"document_number" : "",
	"display_pageid" : "",
	"list_of_parties_and_counsel" : "on",
	"terminated_parties" : "on",
	"pdf_header" : "1",
	"output_format" : "html",
	#"PreResetField" : "",
	#"PreResetFields" : "", 
	"sort1" : "most recent date first",
}

# Have a pretty printer ready
pp = pprint.PrettyPrinter(indent=4)


def get_cookie(name, value, domain = "", path = ""):
	path = path if path != None else ""
	domain = domain if domain != None else ""
	return cookielib.Cookie(version=0, name=name, value='"%s"'%value, port=None, port_specified=False, 
		domain=domain, domain_specified=domain is not None, domain_initial_dot=False, 
		path=path, path_specified=domain is not None, 
		secure=False, expires=None, 
		discard=True, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)

	
def debug_cost_data(soup):
	costs = pacer_parse.parse_cost(soup)
	if costs:
		logging.info("%s - %s: $%.2f"%(costs['user'], costs['time'].strftime("%m/%d/%Y %H:%M:%S"), costs['cost']))
	else:
		logging.warning("No Cost Data: \n" + str(soup))
	return soup
	
		
class DocketDownloader:
	def __init__(self):
		self.cj = cookielib.LWPCookieJar()
		self.urlopen = URLOpener(self.cj)
		self.last_link = None;

		self.cj.clear() # Clear all cookies

	def _get_page(self, req, expect_login=False, download_binary=False):
		'''
		Downloads the page. Mostly just adds debug printing.
		if download_binary is false, then do an initial parse (goes through beautifulsoup)
		'''
		# DEBUG = False
		for (k,v) in default_headers.items():
			req.add_header(k, v)
		if self.last_link:
			req.add_header("Referer", self.last_link)
			
		if DEBUG:
			logging.debug("--- New Request ---")
			logging.debug("Request URL: '%s'"%str(req.get_full_url()))
			logging.debug("Request Type: '%s'"%str(req.get_type()))
			# logging.debug("Origin Host: '%s'"%str(req.get_origin_req_host()))
			for index, cookie in enumerate(self.cj):
				logging.debug("Cookie %d: %s"%(index, cookie))
			logging.debug("Data:\n%s"%str(req.get_data()))
		else:
			logging.debug("Request %s URL: '%s'"%(str(req.get_type()), str(req.get_full_url())))
		
		f = self.urlopen.open(req)
		out = f.read()
		
		if DEBUG:
			info = str(f.info())
			logging.debug("Response Info:\n%s"%info.strip())
			logging.debug("Response Length:%s"%str(len(out)))
		
			for index, cookie in enumerate(self.cj):
				logging.debug("Cookie %d: %s"%(index, cookie))
			if VERBOSE_DEBUG:
				logging.debug(out)
			logging.debug("\n\n")
		
		self.last_link = req.get_full_url()
		
		if f.info().getheader("Content-Type") == "application/pdf":
			download_binary = True
		soup = out if download_binary else InitialParse(out)
		
		# If we expect to be logged in, try again. If that fails, then raise
		if not download_binary and expect_login and not pacer_parse.parse_loginsuccess(soup):
			logging.warning("No PACER login, trying request again")
			soup = self._get_page(req, expect_login = False)
			if not pacer_parse.parse_loginsuccess(soup):
				raise Exception("Could not login to pacer")
		return soup

	def login(self, user, password, matter):
		if TESTING_USE_LOCAL:
			return True
		login_post_data['loginid'] = user
		login_post_data['passwd'] = password
		login_post_data['client'] = matter
		
		# Load the login page to setup cookies and such
		logging.info("Loading the Login Page PACER")
		req = urllib2.Request(login_check_page)
		self._get_page(req)
	
		# Post the actual login
		logging.info("Posting The Login PACER")
		req = urllib2.Request(login_post_page, urllib.urlencode(login_post_data))
		req.add_header("Content-Type", "application/x-www-form-urlencoded")
		results = self._get_page(req)
		return pacer_parse.parse_loginsuccess(results)
	
	def get_session_data(self):
		logging.info("Returning Session Information (%d Cookies)"%(len(self.cj)))
		if DEBUG:
			logging.info("Cookies:\n%s"%(str(self.cj)))
		cookies = []
		for c in self.cj:
			cookies.append(c)
		session_data = base64.b64encode(pickle.dumps(cookies))
		return session_data
	
	def set_session_data(self, data):
		'''
		Instead of logging in manually, set the session data 
		and load a page to check if we logged in
		'''
		self.cj.clear()  # Clear old cookies
		cj = pickle.loads(base64.b64decode(data))
		
		logging.info("Setting PACER Cookies (%d Cookies)"%(len(cj)))
		for c in cj:
			logging.info("    %s"%str(c))
			self.cj.set_cookie(c)
		if DEBUG:
			logging.info("Cookies:\n%s"%(str(self.cj)))

		# Load the login page to setup cookies and such
		logging.info("Loading the Login Page PACER")
		req = urllib2.Request(login_check_page)
		self._get_page(req)			
		results = self._get_page(req)
		logged_in = pacer_parse.parse_loginsuccess(results)
		return logged_in
		
		
	def search(self, docket_num="", party_name="", court_type="", court_region="",
				date_filed_start=None, date_filed_end=None):
		'''
		If court_type is specified, court_name must be as well
		'''
		logging.info("Searching: docket=%s, party=%s, court=%s, region=%s"%(docket_num, 
			party_name, court_type, court_region))
		
		# Setup the Search Paramaters
		search_data = copy.deepcopy(search_post_data_default)
		if court_type:
			search_data['court_type'] = court_type
		if party_name:
			search_data['party'] = party_name
		if docket_num:
			search_data['case_no'] = docket_num
		if court_region:
			search_data.update({
				"all_region": court_region,
				"ap_region": court_region,
				"bk_region": court_region,
				"dc_region": court_region,
			})
		if date_filed_start:
			search_data['date_filed_start'] = date_filed_start
		if date_filed_end:
			search_data['date_filed_end'] = date_filed_end
		# Make the Request
		return self.search_paged(search_post_url, urllib.urlencode(search_data))

	def search_paged(self, link, _encoded_data=None):
		'''
		Return paginated search results. Also used by the normal search
		method above. 
		
		_encoded_data should not be used other than internally
		'''
		logging.info("Searching: link=%s"%link)
		# Make the Request
		req = urllib2.Request(link, _encoded_data)
		req.add_header("Content-Type", "application/x-www-form-urlencoded")
		if TESTING_USE_LOCAL:
			results = open("C:\\Users\\speedplane\\Documents\\Work\\2010 - 2011\\Programs\\DocketUpdater\\test\\SearchResults2.htm").read()
		else:
			results = self._get_page(req, True)
		
		# Parse the result
		soup = debug_cost_data(results)
		cost = pacer_parse.parse_cost(soup)
		search_results, pages = pacer_parse.parse_case_search_results(soup, link)

		# logging.debug("Docket Search Results: " + str(search_results))
		return search_results, pages, cost

	def getdocket_cheaply(self, docket, link, title, link_safe_func, 
			last_get, last_get_val):
		'''
		Returns a docket update but does so less expensively by first checking if there
		are any updates and then pulling the entire docket report.
		'''
		if last_get_val and last_get_val.get('cost') and last_get_val['cost'] > .24:
			# The last update was expensive, so it makes sense to check for
			# changes instead of doing a full update first.
			out = self.has_docket_updated(docket, link, title, last_get)
			if not out['updated']:
				logging.info("No updates since last PACER get, avoiding charges")
				# There has not been an update, so return the old values
				last_get_val['got_cheaply_cost'] = out.get('cost')
				return last_get_val
		
		return self.getdocket(docket, link, title, link_safe_func)
		
	def has_docket_updated(self, docket, link, title, last_update):
		'''
		Used to cheaply determine whether a docket has updated since
		last checking the docket.
		
		last_update is a datetime structure of the last updates
		'''
		if isinstance(last_update, str):
			# Convert to datetime for the lazy
			last_update = datetime.strptime(last_update, "%m/%d/%Y")
		
		out = self.getdocket(docket, link, title, link_safe_func=None,
			_check_updated_date = last_update)
		out.update({
			'updated' : len(out['docket_report']) > 0
		})
		return out
		
	@staticmethod
	def _fix_docket_name(docket):
		# e.g., 0:11-ap-00797 --> 0:11-ap-797
		docket = docket.split("-")
		num = int(docket[-1])
		return "-".join(docket[:-1]) + "-%d"%num
		
	def getdocket(self, docket, link, title, link_safe_func,
			_check_updated_date = None):
		'''
		Pull Docket Information. Will either return the docket information
		or raise an exception. Will not return None.
			title: optional and is only used for debugging
			link: The link to the pacer docket
			link_safe_func: function taking in an href and anchor text and returning a safe link.
			
			_check_updated_date: Do not use directly, instead 
		'''
		# Get the individual docket
		logging.info("Getting Docket For %s (%s)"%(str(title), docket))
		if TESTING_USE_LOCAL:
			results = InitialParse(open("C:\\Users\\speedplane\\Documents\\Work\\2010 - 2011\\Programs\\DocketUpdater\\test\\DocketReport9.htm").read())
			docket_report = pacer_parse.parse_docket_report(results, "http://TESTING.com", link_safe_func)
			return {
					'docket_report' : docket_report,
					'parties' : pacer_parse.parse_docket_parties(results),
					'case_info' : pacer_parse.parse_other_docket_items(results),
					'cost' : pacer_parse.parse_cost(results),
				}
		
		# Pull up the case list
		req = urllib2.Request(link)
		results = self._get_page(req, True)
		
		# Parse the returned page to get all of the case's links
		q = pacer_parse.parse_pacer_query_result(results, link)
		case_menu_links = q.links
		if not case_menu_links.get("Docket Report"):
			raise Exception("No Docket Report Links\n"+str(case_menu_links))
		
		# Get the "run docket report" page
		req = urllib2.Request(case_menu_links["Docket Report"])
		results = self._get_page(req, True)
		# Parse that page so we can get the form submission to parse that link
		(run_docket_link, extra_form_data) = pacer_parse.parse_run_docket_report_page(results, case_menu_links["Docket Report"])
		
		parsed_link = urlparse.urlparse(case_menu_links["Docket Report"])
		case_id = parsed_link.query
		url_path = "/".join(parsed_link.path.split("/")[:-1])  + "/"
		
		cookie_val = "%s(%s)"%(DocketDownloader._fix_docket_name(docket), case_id)
		logging.debug("Setting Cookie: CASE_NUM=" + cookie_val)
		ck = get_cookie("CASE_NUM", cookie_val, parsed_link.netloc, url_path)
		self.cj.set_cookie(ck)	
		
		# Prepare the parameters. Start with what the defaults on the form page
		docket_report_data = copy.deepcopy(extra_form_data)
		# Now add our own defaults, overwriting the form defaults
		docket_report_data.update(docket_report_defaults)
		# Now add the docket specific data
		docket_report_data['case_num'] = DocketDownloader._fix_docket_name(docket)
		docket_report_data['all_case_ids'] = case_id
		docket_report_data['sort1'] = "most recent date first"
		
		# If we're checking for an update, modify the request
		if _check_updated_date:
			# Do not download the counsel information
			del docket_report_data['list_of_parties_and_counsel']
			del docket_report_data['terminated_parties']
			# The date range calculated from when it was entered, not filed
			docket_report_data['date_range_type'] = 'Entered'
			docket_report_data['date_from'] = _check_updated_date.strftime("%m/%d/%Y")
			logging.info(str(docket_report_data))
			
		#logging.info(pp.pformat(docket_report_data))
		# Pacer likes it encoded as 'multipart/form-data'
		datagen, headers = multipart_encode(docket_report_data)
		datagen = "".join(sorted(datagen))
		req = urllib2.Request(run_docket_link, datagen)
		for (k,v) in headers.items():
			req.add_header(k,v)
		results = self._get_page(req, True)

		# Parse the output
		docket_report = pacer_parse.parse_docket_report(results, run_docket_link, link_safe_func)
		parties = pacer_parse.parse_docket_parties(results)
		case_info = pacer_parse.parse_other_docket_items(results)
		cost = pacer_parse.parse_cost(results)
		return {
				'docket_report' : docket_report,
				'parties' : parties,
				'case_info' : case_info,
				'cost' : cost,
			}
	
	def getdocument(self, link):
		'''
		Given a PACER document link, download the document and return it
		'''
		req = urllib2.Request(link)
		results = self._get_page(req, True)
		
		order_page = pacer_parse.parse_try_document_select_parse(results, link)
		if order_page:
			logging.info("Found Document Select Page, Getting Order Form")
			link = order_page
			req = urllib2.Request(link)
			results = self._get_page(req, True)
		
		# Parse the returned page to figure out how to order it
		download_document_link, download_order_data = pacer_parse.parse_download_order_form(results, link)
		cost = pacer_parse.parse_cost(results)
		
		datagen, headers = multipart_encode(download_order_data)
		datagen = "".join(sorted(datagen))
		req = urllib2.Request(download_document_link, datagen)
		for (k,v) in headers.items():
			req.add_header(k,v)
		results = self._get_page(req, True)

		if not IsSoup(results):
			return results, cost
		
		# Parse this page to get the pdf link
		pdf_link = pacer_parse.parse_downloaded_frame(results, download_document_link)
		if not pdf_link:
			raise Exception("Could not find the document pdf.")
		req = urllib2.Request(pdf_link, datagen)
		results = self._get_page(req, False, download_binary=True)
		
		return results, cost

