'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import poster.streaminghttp
import time
import logging
import copy
import urllib
import cookielib
from mimetools import Message

import urllib2, Cookie, urlparse
import BaseHTTPServer

try:
	from google.appengine.api import urlfetch
except ImportError:
	logging.warning("URLFetch Not Found, using urllib2")
	urlfetch = None
VERBOSE = False
HTTPResponses = BaseHTTPServer.BaseHTTPRequestHandler.responses

class StrReadLine():
	def __init__(self, thestr):
		self.thestr = thestr
		self.splitline = thestr.split("\n")
		
	def readline(self):
		if not self.splitline or not len(self.splitline):
			return None
		out = self.splitline[0]
		self.splitline = self.splitline[1:]
		return out
		
	def read(self):
		return self.thestr
	def __repr__(self):
		return self.thestr
	def close(self):
		pass

PACER_TIMEOUT  = 20
SERVER_TIMEOUT = 15*PACER_TIMEOUT
		
class GAEtoURLHeaders():
	def __init__(self, headers):
		self.headers = headers
	
	def __contains__(self, key):
		return self.headers.get(key.lower(), False)

	def getheader(self, key):		
		return self.headers.get(key)
		
	def getheaders(self, key):		
		if "cookie" in key.lower():
			# GAE Bug: http://code.google.com/p/googleappengine/issues/detail?id=3379
			out = self.headers.get(key.lower(), "").split(", ")
		else:
			# This is the correct way of processing headers:
			headers_str =  "\n".join(map(lambda (k,v):"%s: %s"%(k,v), self.headers.items()))
			out = Message(StrReadLine(headers_str)).getheaders(key)
		
		# logging.debug("Header2: %s --> %s"%(key.lower(), str(out)))
		return out
		
		
	def __repr__(self):
		return self.headers.__repr__()
	
class FetchtoResponse():
	def __init__(self, fetch):
		self.fetch = fetch
		if fetch and fetch.status_code >= 400:
			raise urllib2.HTTPError(self.geturl(), 
				fetch.status_code, HTTPResponses.get(fetch.status_code), 
				self.info(), None)
	def close(self):
		pass
	def read(self):
		return self.fetch.content
	def info(self):
		return GAEtoURLHeaders(self.fetch.headers) if self.fetch else {}
	def geturl(self):
		return self.fetch.final_url
	def get_full_url(self):
		return self.fetch.final_url
	
class URLOpener:
	'''
	When using urllib on GAE, cookies don't work.
	Manually setup cookies here.
	'''
	def __init__(self, cj=None):
		#self.cookie = Cookie.SimpleCookie()
		if cj == None:
			cj = cookielib.LWPCookieJar()
		self.cj = cj
		self.cook_proc = urllib2.HTTPCookieProcessor(self.cj)
		self.redirect = poster.streaminghttp.StreamingHTTPRedirectHandler()
		
		if not urlfetch:
			handlers = poster.streaminghttp.get_handlers()
			handlers += [self.cook_proc]
			self.set_openers(handlers)

	def set_openers(self, openers):
		self.openers = openers
		
		if not urlfetch:
			self.urllib_openers = urllib2.build_opener(*self.openers)
			urllib2.install_opener(self.urllib_openers)

	def open(self, req):
		
		SEND_INTERNAL = False
		GAE_DELAY = 0
		INTERNAL_SEND_URL = "http://docketupdate.appspot.com"
		INTERNAL_SEND_URL = "http://www.docketalarm.com"
		INTERNAL_SEND_URL += "/test_%s"%urllib.quote_plus(req.get_full_url())
		if urlfetch:
			while req:
				if req.get_method() == "POST":
					method = urlfetch.POST
				else:
					method = urlfetch.GET
					
				req = self.cook_proc.http_request(req)
				all_headers = req.unredirected_hdrs.copy()
				all_headers.update(req.headers)
				# Make sure GAE doesn't cache our results
				all_headers.update({
					"Pragma":"no-cache", 
					"Cache-Control":"no-cache",
				})
				logging.debug("  -GAE %s-"%req.get_method())
				logging.debug("URL: %s"%req.get_full_url())
				if VERBOSE:
					logging.debug("Headers: %s"%str(all_headers))
					logging.debug("Cookies:\n%s"%str(self.cj))
				if SEND_INTERNAL:
					logging.debug("  -SENDING INTERNAL FIRST-")
					internal_headers = copy.copy(all_headers)
					internal_headers['urlfetch_original_url'] = req.get_full_url()
					fetchresp = urlfetch.fetch(url=INTERNAL_SEND_URL,
						  payload=req.get_data(),
						  method=method,
						  headers=internal_headers,
						  allow_truncated=False,
						  follow_redirects=False,
						  deadline=18,
						  validate_certificate=True,
						  )
				
				fetchresp = None
				for i in range(2):
					try:
						fetchresp = urlfetch.fetch(url=req.get_full_url(),
							  payload=req.get_data(),
							  method=method,
							  headers=all_headers,
							  allow_truncated=False,
							  follow_redirects=False,
							  deadline=SERVER_TIMEOUT
							  )
					except urlfetch.DownloadError, e:
						if i == 0:
							# Try to determine whether the server is even awake
							host_url = "http://" + req.get_host()
							try:
								# Don't set fetchresp, we're checking if the server is alive
								urlfetch.fetch(url=host_url, 
									deadline=SERVER_TIMEOUT)
							except urlfetch.DownloadError, e:
								raise Exception("Cannot access %s, please try again later"%req.get_host())
							except Exception, e:
								logging.error("Second URLFetch Error: " + str(e))
								raise
					except Exception, e:
						logging.error("URLFetch Error: " + str(e))
						raise
					else:
						break
					logging.warning("Trying to send again")
				if GAE_DELAY:
					time.sleep(GAE_DELAY)
				resp = FetchtoResponse(fetchresp)
				# Process Cookies
				resp = self.cook_proc.http_response(req, resp)
					
				req = self._handle_redirect(req, fetchresp.status_code,
					headers=resp.info())
		else:
			# Clear the urllib cache
			urllib.urlcleanup()
			all_headers = req.unredirected_hdrs.copy()
			all_headers.update(req.headers)
			logging.debug("  -URLLIB %s-"%req.get_method() )
			logging.debug("URL: %s"%req.get_full_url())
			if VERBOSE:
				logging.debug("Headers: %s"%str(all_headers))
			if SEND_INTERNAL:
				logging.debug("  -SENDING INTERNAL FIRST-")
				internal_headers = copy.copy(all_headers)
				internal_headers['urlfetch_original_url'] = req.get_full_url()	
				req2 = urllib2.Request(INTERNAL_SEND_URL,
						data=req.get_data(), headers=internal_headers)
				urllib2.urlopen(req2)
			try:
				resp = urllib2.urlopen(req, timeout=PACER_TIMEOUT)
			except urllib2.URLError, e:
				if "timed out" in str(e):
					raise Exception("Cannot access %s, please try again later"%req.get_host())
				raise
			
		return resp
		
	def _handle_redirect(self, req, code, headers):
		m =  req.get_method()
		if not (code in (301, 302, 303, 307) and m in ("GET", "HEAD")
				or code in (301, 302, 303) and m == "POST"):
			return None
		
		if 'location' in headers:
			newurl = headers.getheaders('location')[0]
		elif 'uri' in headers:
			newurl = headers.getheaders('uri')[0]
		else:
			logging.warning("Found redirect code %s without redirect location"%str(code))
			return
		newurl = urlparse.urljoin(req.get_full_url(), newurl)
		all_headers = req.unredirected_hdrs.copy()
		all_headers.update(req.headers)
		logging.debug("Handling Redirect (%s) to: %s"%(str(code), (newurl)))
		return urllib2.Request(newurl,
			headers=all_headers,
			origin_req_host=req.get_origin_req_host(),
			unverifiable=True)
