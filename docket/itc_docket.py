'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import sys
import os
import logging
import urllib
import urllib2
import base64
import pprint
import copy
import re
from collections import defaultdict
from datetime import datetime

if __name__ == "__main__":
	logging.basicConfig(
		level = logging.DEBUG,
		format = '%(asctime)s %(levelname)s %(message)s',
	)
	logging.debug("Testing Logging")
	ROOT_PATH = os.path.dirname(__file__)
	sys.path.append(ROOT_PATH)
	sys.path.append(os.path.join(ROOT_PATH, '../'))
import docket
import xml.etree.ElementTree as ET

TESTING_USE_LOCAL = False
BASE_URL = "https://edis.usitc.gov/data"

docket.DEBUG = True
docket.VERBOSE_DEBUG = False

class ITCDocketDownloader(docket.DocketDownloader):
	def __init__(self):
		docket.DocketDownloader.__init__(self)
		self.AuthenticationToken = None
		self.secretKey = None
		self.username = None
	
	def _get_page(self, req, download_binary=False):
		'''
		Download the page.
		BeautifulSoup is broken for XML (empty tags) so we have to use something else
		'''
		out = docket.DocketDownloader._get_page(self, req, expect_login=False, download_binary=True)
		if download_binary:
			return out 
		try:
			xml = ET.XML(out)
		except Exception, e:
			logging.error(str(e))
			raise
		return xml
		
	
	def _add_token_headers(self, req):
		if not self.secretKey or not self.AuthenticationToken:
			raise Exception("No Authorization Token Created")
		req.add_header("Authorization", self.AuthenticationToken)
	
	def _check_error(self, soup):
		error_list = map(lambda s: unicode(s.text), soup.findall("error"))
		if error_list:
			raise Exception("Error Downloading ITC information: " + "\n".join(error_list))
	
	def _put_xml_into_dict(self, soup, result_to_xml):
		'''
		Convert an xml representation (soup) into a dictionary using
		the mapping result_to_xml. Case insensitive on the keys for
		beautifulsoup.
		'''
		res = {}
		for (k,v) in result_to_xml.items():
			s = soup.find(v)
			if s is None:
				raise Exception("Cannot Find: %s in: \n%s "%(v, ET.tostring(soup)))
			res[k] = unicode(s.text) if s.text is not None else None
		return res
	
	@staticmethod
	def _get_contents(d):
		'''
		Return the 'docket text' of the given docket entry.
		We make this docket text up but we try to do a decent job at it.
		
		FIXME/WARNING: Every time the docket text is changed, it also changes the last update
		'''
		contents = ""
		# Wrap all of the vaues in a span so we can locate them and mess with them using css
		d = copy.deepcopy(d)
		for (k, v) in d.items():
			d[k] = '<span class="docket_%s">%s</span>'%(k,v)
		if d['title']:
			contents += "%(security)s %(type)s -- %(title)s."%d
		else:
			contents += "%(security)s %(type)s."%d
		contents += "<br>Filed by %(filer)s, %(firm)s, for %(party)s."%d
		return contents
		
	def login(self, username="DocketAlarm", password="$1234abcd"):
		if TESTING_USE_LOCAL:
			return True
			
		Auth_URL = BASE_URL + "/secretKey/%s"%username
		login_post_data = {'password' : password }
		
		# Load the login page to setup cookies and such
		logging.info("Loading the Login Page EDIS")
		req = urllib2.Request(Auth_URL, urllib.urlencode(login_post_data))
		soup = self._get_page(req)
		self._check_error(soup)
		
		# The docs say secretKey, but soup lowers everything
		soup = soup.find("secretKey")
		if soup is None:
			raise Exception("Could not login, authentication token not found")
		self.secretKey = soup.text
		self.username = username
		
		RawAuthenticationToken = username + ":" + self.secretKey
		self.AuthenticationToken = "Basic " + base64.b64encode((RawAuthenticationToken))

		return None

	def get_session_data(self):
		'''
		Save our login information
		'''
		return {
			'username' : self.username,
			'secretKey' : self.secretKey,
			'AuthenticationToken' : self.AuthenticationToken,
		}
		
	def set_session_data(self, data):
		'''
		Load our login information
		'''
		self.username = data.get('username')
		self.secretKey = data.get('secretKey')
		self.AuthenticationToken = data.get('AuthenticationToken')
	
	@staticmethod
	def _fix_title(title, inv_num):
		import BlueBook
		(first, mid, last) = inv_num.partition("-")
		if mid:
			title = title.replace("%s-TA-%s"%(first, last), "")
		title = title.replace(inv_num, "")
		title = title.replace("Inv. No.", "").strip()

		while title[-1] in ";,":
			title = title[:-1].strip()
		
		title = BlueBook.abbreviate_title(title)
		return title
		
	def search(self, investigation_num=None, phase=None, type=None, status=None):
		'''
		Search for ITC investigations
			status can be: Preinstitution, Active, Inactive, Cancelled
		'''
		Search_URL = BASE_URL + "/investigation"
		if investigation_num:
			Search_URL += "/" + investigation_num
		if phase:
			Search_URL += "/" + phase
		get_param = {}
		if type:
			get_param['investigationType'] = type
		if status:
			get_param['investigationStatus'] = status
		if get_param:
			Search_URL += "?" + urllib.urlencode(get_param)
			
		req = urllib2.Request(Search_URL)
		self._add_token_headers(req)
		soup = self._get_page(req)
		self._check_error(soup)
		
		result_to_xml = {
			'docket':	"investigationNumber",
			'phase' : 	"investigationPhase",
			'status' : 	"investigationStatus",
			'title' : 	"investigationTitle",
			'type' : 	"investigationType",
			'link' : 	"documentListUri",
		}
		out = map(lambda d: self._put_xml_into_dict(d, result_to_xml),
							soup.findall("investigations/investigation"))
		for o in out:
			o['court_name'] = 'United States International Trade Commission'
			o['court_id'] = 'ITC'
			o['title'] = ITCDocketDownloader._fix_title(o['title'], investigation_num)
			
		return out
	@staticmethod
	def _get_info_from_docket(docket):
		info = {}
		for d in docket:
			# logging.info(d['party'])
			if d['party'] == "Administrative Law Judge":
				info['judge'] = d['filer']
		return info
	
	@staticmethod
	def _get_counsel_from_docket(docket):
		'''
		The ITC does not give us clean party information. Instead we parse
		the docket entries to find them. Unfortunately, the docket entries are
		rarely consistent, so we try to do the best we can
		'''
		party_to_counsel = {}
		company_parts = ["inc", "corp", "llc", "ltd", "holdings", "lp"]
		def conv(x):
			'''
			Put the input string into canonical form by deleting a number of common items
			'''
			x = x.lower().replace(".","").replace(",","")
			to_replace = [("incorporated","inc"), ("corporation", "corp"), 
							("technology","tech"), ("et al", "")]
			to_replace += map(lambda x: (x, ""), company_parts)
			for (old, new) in to_replace:
				x = re.sub("^%s| %s"%(old,old), new, x)
			return x.replace(" ","")
		
		# First get the list of parties
		parties_as_named = set([])
		party_to_counsel = defaultdict(lambda : {'type':'','name':'','counsel':[]})
		for d in docket:
			if d['firm'].lower().replace(".","") in ["usitc", "itc"]:
				continue
			# Parties names are normally split by commas and 'and'
			# The most common case is "x, y and z" but there are lots of corner cases. E.g.:
			#		"x, inc., y and z"   "x, y, and z"   "x; y and z"
			dparties = []
			for p in re.split(", |; ", d['party']):
				# Handle: foo Inc. and bar.
				pspaced = p.split(" ")
				while dparties and pspaced and conv(pspaced[0]) in company_parts:
					dparties[-1] += " " + pspaced[0]
					pspaced = pspaced[1:]
				if not p:
					continue
				p = " ".join(pspaced)
				
				# Handle: "a, inc" by combining the inc with the a
				if dparties and conv(p) in company_parts:
					dparties[-1] += ", " + p
					continue
				
				# Handle: "x and y" and "x, and y". This breaks "Barnes and Nobles"
				for p2 in re.split(r' and |^and ', p):
					if conv(p2) and conv(p2) not in company_parts:
						dparties.append(p2)
			# Now that we have all of the party names, build the structure
			counsel = {'name':d['filer'], 'other':[d['firm']]}
			for p in dparties:
				party = party_to_counsel[conv(p)]
				if counsel not in party['counsel']:
					party['counsel'].append(counsel)
				party['name'] = p
				if d['type'] == 'Complaint':
					party['type'] = 'Complainant'
		
		# Merge the parties into the shortest party name
		# This is an O(N^2) algorithm, so we add arbitrary limit
		for k1, v1 in party_to_counsel.items():
			for k2, v2 in party_to_counsel.items()[:500]:
				if k1 is k2 or not k2 or not k1:
					continue
				if k1 in k2: # k1 is the shorter name, delete k2
					# Combine the counsel list
					canonical_k1_counsel = map(lambda c: conv(c['name']), party_to_counsel[k1]['counsel'])
					for counsel in party_to_counsel[k2]['counsel']:
						# Key off the counsel's canonical name
						if conv(counsel['name']) not in canonical_k1_counsel:
							party_to_counsel[k1]['counsel'].append(counsel)
					party_to_counsel[k2]['counsel'] = []
					if party_to_counsel[k2]['type'] == 'Complainant':
						party_to_counsel[k1]['type'] = 'Complainant'
					party_to_counsel[k2]['name'] = None
		
		# Now remove all the empty parties
		out = filter(lambda x: x['name'] != None, party_to_counsel.values())
		return sorted(out, key=lambda x:x['name'].lower())
		
	def getdocket(self, docket=None, title=None, 
					phase=None, firmOrg=None, docType=None, 
					securityLevel=None, limit=None,
					link_safe_func = None):
		'''
		Note that title is optional and is only used for debugging
		'''
		page = 1
		out_docket = []
		while(True):
			Docket_URL = BASE_URL + "/document"
			get_param = {
				'pageNumber' : str(page),
			}
			if docket:
				get_param['investigationNumber'] = docket
			if phase:
				get_param['investigationPhase'] = phase
			if docType:
				get_param['documentType'] = docType
			if firmOrg:
				get_param['firmOrg'] = firmOrg
			if securityLevel:
				get_param['securityLevel'] = securityLevel
			
			Docket_URL += "?" + urllib.urlencode(get_param)
				
			req = urllib2.Request(Docket_URL)
			self._add_token_headers(req)
			soup = self._get_page(req)
			self._check_error(soup)	
			
			result_to_xml = {
				'docid' 	:	'id',
				'type' 		:	'documentType',
				'title' 	:	'documentTitle',
				'security' 	:	'securityLevel',
				'docket'	:	'investigationNumber',
				'phase'		:	'investigationPhase',
				'status'	: 	'investigationStatus',
				'inv_title'	:	'investigationTitle',
				'firm'		:	'firmOrganization',
				'filer'		:	'filedBy',
				'party'		:	'onBehalfOf',
				'doc_date'	:	'documentDate',
				'date'		:	'officialReceivedDate',
				'attach_link':	'attachmentListUri',
			}
			documents = soup.findall("documents/document")
			for d in documents:
				doc = self._put_xml_into_dict(d, result_to_xml)
				doc['number'] = int(doc['docid'])
				# Set a link only if the document is public
				doc['link'] = link_safe_func(
					href = "", 
					filing_number = doc['docid'], exhibit_num = "1"
					) if link_safe_func and doc['security'].lower() == "public" else None
				doc['contents'] = ITCDocketDownloader._get_contents(doc)
				
				doc['date'] = doc['date'].split(".")[0] # Remove microseconds from the date
				doc['date'] = datetime.strptime(doc['date'], "%Y-%m-%d %H:%M:%S")
				
				doc['doc_date'] = doc['doc_date'].split(".")[0] # Remove microseconds from the date
				doc['doc_date'] = datetime.strptime(doc['doc_date'], "%Y-%m-%d %H:%M:%S")
				
				out_docket.append(doc)
			if len(documents) < 100:
				break
			if limit and len(out_docket) >= limit:
				# Just return what we have rather than limiting the results
				break
			if page >= 30:
				logging.error("Too many filings, truncating result")
				break
			page += 1
		return {	'docket_report' : out_docket,
					'parties' : ITCDocketDownloader._get_counsel_from_docket(out_docket),
					'case_info' : ITCDocketDownloader._get_info_from_docket(out_docket),
				}
	
	def getdocumentattachments(self, docid=None, link=None, link_safe_func = None):
		'''
		The link is to the document attachment meta-data, not the data itself
		
		must specify either a link or docid
		'''
		if link and docid:
			raise Exception("Must specify either a link or a docket id, not both.")
		if docid:
			docid = str(docid) # Convert integers
		if docid and "https" in docid: 
			# A sanity check more than anything else
			raise Exception("Confusing a link with a docket id.")
		if link is None:
			link = BASE_URL + "/attachment/" + str(docid)
		req = urllib2.Request(link)
		self._add_token_headers(req)
		soup = self._get_page(req)
		self._check_error(soup)	
		
		result_to_xml = {
			'attachid' 	:	'id',
			'docid'		:	'documentId',
			'title' 	:	'title',
			'size'		:	'fileSize',
			'filename'	:	'originalFileName',
			'pages'		: 	'pageCount',
			'date'		:	'createDate',	# Date uploaded, not filed
			'modified'	:	'lastModifiedDate', # used by OCR
			'edis_link'	:	'downloadUri',
		}
		
		out = map(lambda d: self._put_xml_into_dict(d, result_to_xml),
							soup.findall("attachments/attachment"))
		if link_safe_func:
			for i, o in enumerate(out):
				o['exhibit'] = i+1
				o['link'] = link_safe_func(
						href = o['edis_link'], 
						filing_number = o['docid'], 
						exhibit_num = i+1
						) if link_safe_func else None
				o['date'] = o['date'].split(".")[0] # Remove microseconds from the date
				o['date'] = datetime.strptime(o['date'], "%Y-%m-%d %H:%M:%S")

		return out
		
	def getdocument(self, download_uri):
		'''
		The link should be to the actual data. 
		It can be retrieved using getdocumentattachments.
		'''
		req = urllib2.Request(download_uri)
		self._add_token_headers(req)
		return self._get_page(req, download_binary=True)
		
	
if __name__ == "__main__":
	pp = pprint.PrettyPrinter(indent=4)
	dd = ITCDocketDownloader()
	dd.login("", "")
	
	if True:
		logging.info("Performing Search")
		search = dd.search("731-1103", phase="final")
		logging.info("Search Result")
		pp.pprint(search)
		logging.info("")
		logging.info("")
	
	pp.pprint(dd.getdocket("337-827", limit=150))
	pp.pprint(dd.getdocumentattachments("466874 "))
