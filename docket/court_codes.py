'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
CourtCodes = {   '01bap': (   'U.S. Court Of Appeals, First Circuit BAP',
                 '02/10/1996',
                 '11/08/2011'),
    '01cae': (   'U.S. Court of Appeals, First Circuit',
                 '01/01/1990',
                 '11/10/2011'),
    '02cae': (   'U.S. Court of Appeals, Second Circuit',
                 '09/07/1993',
                 '11/10/2011'),
    '03cae': (   'U.S. Court of Appeals, Third Circuit',
                 '05/30/1974',
                 '11/10/2011'),
    '04cae': (   'U.S. Court of Appeals, Fourth Circuit',
                 '05/30/1990',
                 '11/10/2011'),
    '05cae': (   'U.S. Court of Appeals, Fifth Circuit',
                 '01/01/1960',
                 '11/09/2011'),
    '06cae': (   'U.S. Court of Appeals, Sixth Circuit',
                 '06/13/1977',
                 '11/10/2011'),
    '07cae': (   'U.S. Court of Appeals, Seventh Circuit',
                 '08/21/1989',
                 '11/09/2011'),
    '08cae': (   'U.S. Court of Appeals, Eighth Circuit',
                 '07/25/1983',
                 '11/09/2011'),
    '09bap': (   'U.S. Court Of Appeals, Ninth Circuit BAP',
                 '07/18/1985',
                 '11/09/2011'),
    '09cae': (   'U.S. Court of Appeals, Ninth Circuit',
                 '05/12/1950',
                 '11/09/2011'),
    '10bap': (   'U.S. Court Of Appeals, Tenth Circuit BAP',
                 '02/10/1996',
                 '11/04/2011'),
    '10cae': (   'U.S. Court of Appeals, Tenth Circuit',
                 '01/01/1978',
                 '11/09/2011'),
    '11cae': (   'U.S. Court of Appeals, Eleventh Circuit',
                 '01/01/2010',
                 '11/10/2011'),
    'akbke': ('Alaska Bankruptcy Court', '01/02/1991', '11/09/2011'),
    'akdce': ('Alaska District Court', '02/27/1960', '11/07/2011'),
    'almbke': ('Alabama Middle Bankruptcy Court', '01/03/1989', '11/09/2011'),
    'almdce': ('Alabama Middle District Court', '09/09/1966', '11/09/2011'),
    'alnbke': (   'Alabama Northern Bankruptcy Court',
                  '01/03/1989',
                  '11/09/2011'),
    'alndce': ('Alabama Northern District Court', '03/11/1963', '11/09/2011'),
    'alsbke': (   'Alabama Southern Bankruptcy Court',
                  '01/02/1991',
                  '11/09/2011'),
    'alsdce': ('Alabama Southern District Court', '03/27/1963', '11/09/2011'),
    'arebke': (   'Arkansas Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'aredce': ('Arkansas Eastern District Court', '07/08/1966', '11/09/2011'),
    'arwbke': (   'Arkansas Western Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'arwdce': ('Arkansas Western District Court', '11/05/1969', '11/09/2011'),
    'azbke': ('Arizona Bankruptcy Court', '01/05/1987', '11/09/2011'),
    'azdce': ('Arizona District Court', '08/01/1958', '11/09/2011'),
    'cacbke': (   'California Central Bankruptcy Court',
                  '01/02/1985',
                  '11/10/2011'),
    'cacdce': (   'California Central District Court',
                  '10/26/1954',
                  '11/09/2011'),
    'caebke': (   'California Eastern Bankruptcy Court',
                  '01/07/1985',
                  '11/09/2011'),
    'caedce': (   'California Eastern District Court',
                  '06/04/1953',
                  '11/09/2011'),
    'canbke': (   'California Northern Bankruptcy Court',
                  '01/02/1991',
                  '11/09/2011'),
    'candce': (   'California Northern District Court',
                  '12/22/1956',
                  '11/09/2011'),
    'casbk': (   'California Southern Bankruptcy Court',
                 '01/05/1987',
                 '07/28/2000'),
    'casbke': (   'California Southern Bankruptcy Court',
                  '01/02/1990',
                  '11/09/2011'),
    'casdce': (   'California Southern District Court',
                  '01/25/1951',
                  '11/09/2011'),
    'cobke': ('Colorado Bankruptcy Court', '01/03/1990', '11/09/2011'),
    'codce': ('Colorado District Court', '08/08/1950', '11/09/2011'),
    'cofce': (   'United States Federal Claims Court',
                 '01/01/1981',
                 '11/10/2011'),
    'ctbke': ('Connecticut Bankruptcy Court', '01/02/1986', '11/10/2011'),
    'ctdce': ('Connecticut District Court', '01/01/1961', '11/10/2011'),
    'dcbke': (   'District Of Columbia Bankruptcy Court',
                 '01/02/1990',
                 '11/10/2011'),
    'dccae': (   'U.S. Court of Appeals, D.C. Circuit',
                 '04/26/1968',
                 '11/10/2011'),
    'dcdce': (   'District Of Columbia District Court',
                 '06/05/1950',
                 '11/10/2011'),
    'debke': ('Delaware Bankruptcy Court', '01/03/1989', '11/11/2011'),
    'dedce': ('Delaware District Court', '06/17/1966', '11/10/2011'),
    'flmbke': ('Florida Middle Bankruptcy Court', '01/01/1980', '11/09/2011'),
    'flmdce': ('Florida Middle District Court', '01/01/1950', '11/10/2011'),
    'flnbke': (   'Florida Northern Bankruptcy Court',
                  '01/01/1980',
                  '11/10/2011'),
    'flndce': ('Florida Northern District Court', '02/01/1960', '11/10/2011'),
    'flsbke': (   'Florida Southern Bankruptcy Court',
                  '01/08/1985',
                  '11/10/2011'),
    'flsdce': ('Florida Southern District Court', '01/29/1957', '11/10/2011'),
    'gambke': ('Georgia Middle Bankruptcy Court', '01/26/1986', '11/09/2011'),
    'gamdce': ('Georgia Middle District Court', '01/13/1964', '11/10/2011'),
    'ganbke': (   'Georgia Northern Bankruptcy Court',
                  '01/02/1992',
                  '11/10/2011'),
    'gandce': ('Georgia Northern District Court', '01/26/1950', '11/10/2011'),
    'gasbke': (   'Georgia Southern Bankruptcy Court',
                  '07/11/1985',
                  '11/10/2011'),
    'gasdce': ('Georgia Southern District Court', '04/12/1952', '11/10/2011'),
    'gubke': ('Guam Bankruptcy Court', '01/01/1994', '11/08/2011'),
    'gudce': ('Guam District Court', '10/28/1954', '11/09/2011'),
    'hibke': ('Hawaii Bankruptcy Court', '01/02/1991', '11/09/2011'),
    'hidce': ('Hawaii District Court', '08/29/1955', '11/09/2011'),
    'ianbke': ('Iowa Northern Bankruptcy Court', '01/02/1992', '11/09/2011'),
    'iandce': ('Iowa Northern District Court', '03/03/1967', '11/09/2011'),
    'iasbke': ('Iowa Southern Bankruptcy Court', '01/02/1990', '11/09/2011'),
    'iasdce': ('Iowa Southern District Court', '08/09/1973', '11/09/2011'),
    'idbke': ('Idaho Bankruptcy Court', '01/02/1986', '11/09/2011'),
    'iddce': ('Idaho District Court', '03/17/1976', '11/09/2011'),
    'ilcbke': (   'Illinois Central Bankruptcy Court',
                  '01/03/1989',
                  '11/09/2011'),
    'ilcdce': ('Illinois Central District Court', '12/12/1973', '11/09/2011'),
    'ilnbke': (   'Illinois Northern Bankruptcy Court',
                  '01/02/1992',
                  '11/10/2011'),
    'ilndce': ('Illinois Northern District Court', '08/29/1955', '11/09/2011'),
    'ilsbke': (   'Illinois Southern Bankruptcy Court',
                  '01/03/1989',
                  '11/09/2011'),
    'ilsdce': ('Illinois Southern District Court', '12/14/1977', '11/09/2011'),
    'innbke': (   'Indiana Northern Bankruptcy Court',
                  '01/04/1988',
                  '11/10/2011'),
    'inndce': ('Indiana Northern District Court', '05/23/1968', '11/10/2011'),
    'insbke': (   'Indiana Southern Bankruptcy Court',
                  '11/01/1974',
                  '11/11/2011'),
    'insdce': ('Indiana Southern District Court', '08/18/1975', '11/10/2011'),
    'ksbke': ('Kansas Bankruptcy Court', '01/02/1986', '11/09/2011'),
    'ksdce': ('Kansas District Court', '02/28/1951', '11/09/2011'),
    'kyebke': (   'Kentucky Eastern Bankruptcy Court',
                  '01/03/1984',
                  '11/10/2011'),
    'kyedce': ('Kentucky Eastern District Court', '11/06/1974', '11/10/2011'),
    'kywbke': (   'Kentucky Western Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'kywdce': ('Kentucky Western District Court', '07/18/1973', '11/10/2011'),
    'laebke': (   'Louisiana Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'laedce': ('Louisiana Eastern District Court', '07/30/1964', '11/09/2011'),
    'lambke': (   'Louisiana Middle Bankruptcy Court',
                  '01/02/1992',
                  '11/09/2011'),
    'lamdce': ('Louisiana Middle District Court', '09/04/1952', '11/09/2011'),
    'lawbke': (   'Louisiana Western Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'lawdce': ('Louisiana Western District Court', '12/02/1964', '11/09/2011'),
    'mabke': ('Massachusetts Bankruptcy Court', '01/02/1991', '11/10/2011'),
    'madce': ('Massachusetts District Court', '08/29/1961', '11/10/2011'),
    'mdbke': ('Maryland Bankruptcy Court', '01/02/1986', '11/09/2011'),
    'mddce': ('Maryland District Court', '03/07/1970', '11/10/2011'),
    'mebke': ('Maine Bankruptcy Court', '01/03/1989', '11/10/2011'),
    'medce': ('Maine District Court', '07/01/1972', '11/10/2011'),
    'miebke': (   'Michigan Eastern Bankruptcy Court',
                  '03/13/1985',
                  '11/09/2011'),
    'miedce': ('Michigan Eastern District Court', '06/01/1956', '11/10/2011'),
    'miwbke': (   'Michigan Western Bankruptcy Court',
                  '01/04/1988',
                  '11/10/2011'),
    'miwdce': ('Michigan Western District Court', '01/01/1970', '11/10/2011'),
    'mnbke': ('Minnesota Bankruptcy Court', '01/02/1992', '11/09/2011'),
    'mndce': ('Minnesota District Court', '09/28/1970', '11/09/2011'),
    'moebke': (   'Missouri Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'moedce': ('Missouri Eastern District Court', '01/05/1961', '11/09/2011'),
    'mowbke': (   'Missouri Western Bankruptcy Court',
                  '01/04/1988',
                  '11/09/2011'),
    'mowdce': ('Missouri Western District Court', '01/01/1950', '11/09/2011'),
    'msnbke': (   'Mississippi Northern Bankruptcy Court',
                  '01/04/1988',
                  '11/09/2011'),
    'msndce': (   'Mississippi Northern District Court',
                  '04/22/1964',
                  '11/09/2011'),
    'mssbke': (   'Mississippi Southern Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'mssdce': (   'Mississippi Southern District Court',
                  '03/07/1963',
                  '11/09/2011'),
    'mtbke': ('Montana Bankruptcy Court', '01/02/1986', '11/09/2011'),
    'mtdce': ('Montana District Court', '01/14/1965', '11/09/2011'),
    'ncebke': (   'North Carolina Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'ncedce': (   'North Carolina Eastern District Court',
                  '02/25/1964',
                  '11/10/2011'),
    'ncmbke': (   'North Carolina Middle Bankruptcy Court',
                  '02/10/1989',
                  '11/10/2011'),
    'ncmdce': (   'North Carolina Middle District Court',
                  '06/04/1971',
                  '11/10/2011'),
    'ncwbke': (   'North Carolina Western Bankruptcy Court',
                  '01/01/1988',
                  '11/10/2011'),
    'ncwdce': (   'North Carolina Western District Court',
                  '02/03/1956',
                  '11/10/2011'),
    'ndbke': ('North Dakota Bankruptcy Court', '01/02/1992', '11/09/2011'),
    'nddce': ('North Dakota District Court', '09/26/1980', '11/07/2011'),
    'nebke': ('Nebraska Bankruptcy Court', '01/02/1990', '11/09/2011'),
    'nedce': ('Nebraska District Court', '10/15/1969', '11/09/2011'),
    'nhbke': ('New Hampshire Bankruptcy Court', '01/03/1989', '11/10/2011'),
    'nhdce': ('New Hampshire District Court', '09/21/1966', '11/10/2011'),
    'njbke': ('New Jersey Bankruptcy Court', '01/02/1980', '11/09/2011'),
    'njdce': ('New Jersey District Court', '09/22/1969', '11/10/2011'),
    'nmbke': ('New Mexico Bankruptcy Court', '01/16/1991', '11/09/2011'),
    'nmdce': ('New Mexico District Court', '04/20/1966', '11/09/2011'),
    'nmidce': (   'Northern Mariana Islands District Court',
                  '07/05/1991',
                  '11/04/2011'),
    'nvbke': ('Nevada Bankruptcy Court', '01/03/1984', '11/09/2011'),
    'nvdce': ('Nevada District Court', '02/09/1953', '11/09/2011'),
    'nyebke': (   'New York Eastern Bankruptcy Court',
                  '01/02/1985',
                  '11/11/2011'),
    'nyedce': ('New York Eastern District Court', '07/23/1962', '11/10/2011'),
    'nynbke': (   'New York Northern Bankruptcy Court',
                  '05/01/1992',
                  '11/10/2011'),
    'nyndce': ('New York Northern District Court', '02/05/1970', '11/10/2011'),
    'nysbke': (   'New York Southern Bankruptcy Court',
                  '01/03/1989',
                  '11/10/2011'),
    'nysdce': ('New York Southern District Court', '01/21/1952', '11/09/2011'),
    'nywbke': (   'New York Western Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'nywdce': ('New York Western District Court', '06/26/1972', '11/10/2011'),
    'ohnbke': ('Ohio Northern Bankruptcy Court', '01/02/1985', '11/09/2011'),
    'ohndce': ('Ohio Northern District Court', '06/26/1968', '11/09/2011'),
    'ohsbke': ('Ohio Southern Bankruptcy Court', '01/01/1991', '11/09/2011'),
    'ohsdce': ('Ohio Southern District Court', '06/27/1972', '11/10/2011'),
    'okebke': (   'Oklahoma Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'okedce': ('Oklahoma Eastern District Court', '04/24/1972', '11/10/2011'),
    'oknbke': (   'Oklahoma Northern Bankruptcy Court',
                  '01/02/1991',
                  '11/11/2011'),
    'okndce': ('Oklahoma Northern District Court', '06/22/1976', '11/10/2011'),
    'okwbke': (   'Oklahoma Western Bankruptcy Court',
                  '07/24/1992',
                  '11/10/2011'),
    'okwdce': ('Oklahoma Western District Court', '01/04/1978', '11/10/2011'),
    'orbke': ('Oregon Bankruptcy Court', '01/02/1985', '11/09/2011'),
    'ordce': ('Oregon District Court', '01/23/1962', '11/09/2011'),
    'paebke': (   'Pennsylvania Eastern Bankruptcy Court',
                  '01/03/1989',
                  '11/10/2011'),
    'paedce': (   'Pennsylvania Eastern District Court',
                  '06/21/1970',
                  '11/09/2011'),
    'pambke': (   'Pennsylvania Middle Bankruptcy Court',
                  '01/02/1990',
                  '11/10/2011'),
    'pamdce': (   'Pennsylvania Middle District Court',
                  '03/01/1964',
                  '11/10/2011'),
    'pawbke': (   'Pennsylvania Western Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'pawdce': (   'Pennsylvania Western District Court',
                  '01/02/1950',
                  '11/10/2011'),
    'prbke': ('Puerto Rico Bankruptcy Court', '01/05/1987', '11/10/2011'),
    'prdce': ('Puerto Rico District Court', '01/29/1969', '11/10/2011'),
    'ribke': ('Rhode Island Bankruptcy Court', '01/02/1991', '11/10/2011'),
    'ridce': ('Rhode Island District Court', '02/05/1971', '11/10/2011'),
    'scbke': ('South Carolina Bankruptcy Court', '01/03/1989', '11/10/2011'),
    'scdce': ('South Carolina District Court', '05/29/1962', '11/10/2011'),
    'sdbke': ('South Dakota Bankruptcy Court', '01/02/1992', '11/10/2011'),
    'sddce': ('South Dakota District Court', '01/31/1950', '11/10/2011'),
    'tnebke': (   'Tennessee Eastern Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'tnedce': ('Tennessee Eastern District Court', '10/27/1982', '11/10/2011'),
    'tnmbke': (   'Tennessee Middle Bankruptcy Court',
                  '01/02/1992',
                  '11/10/2011'),
    'tnmdce': ('Tennessee Middle District Court', '09/23/1955', '11/10/2011'),
    'tnwbke': (   'Tennessee Western Bankruptcy Court',
                  '01/02/1991',
                  '11/10/2011'),
    'tnwdce': ('Tennessee Western District Court', '06/12/1963', '11/10/2011'),
    'txebke': ('Texas Eastern Bankruptcy Court', '01/02/1987', '11/10/2011'),
    'txedce': ('Texas Eastern District Court', '01/09/1964', '11/10/2011'),
    'txnbke': ('Texas Northern Bankruptcy Court', '01/02/1987', '11/09/2011'),
    'txndce': ('Texas Northern District Court', '11/25/1952', '11/10/2011'),
    'txsbke': ('Texas Southern Bankruptcy Court', '05/31/1991', '11/09/2011'),
    'txsdce': ('Texas Southern District Court', '05/06/1951', '11/09/2011'),
    'txwbke': ('Texas Western Bankruptcy Court', '01/04/1988', '11/09/2011'),
    'txwdce': ('Texas Western District Court', '08/20/1970', '11/10/2011'),
    'utbke': ('Utah Bankruptcy Court', '01/02/1985', '11/10/2011'),
    'utdce': ('Utah District Court', '09/19/1951', '11/09/2011'),
    'vaebke': (   'Virginia Eastern Bankruptcy Court',
                  '02/11/1988',
                  '11/10/2011'),
    'vaedce': ('Virginia Eastern District Court', '12/13/1968', '11/10/2011'),
    'vawbke': (   'Virginia Western Bankruptcy Court',
                  '01/02/1985',
                  '11/10/2011'),
    'vawdce': ('Virginia Western District Court', '05/22/1978', '11/10/2011'),
    'vibke': ('Virgin Islands Bankruptcy Court', '01/03/1989', '10/24/2011'),
    'vidce': ('Virgin Islands', '01/08/1970', '11/09/2011'),
    'vtbke': ('Vermont Bankruptcy Court', '01/02/1991', '11/10/2011'),
    'vtdce': ('Vermont District Court', '12/05/1974', '11/10/2011'),
    'waebke': (   'Washington Eastern Bankruptcy Court',
                  '01/02/1991',
                  '11/09/2011'),
    'waedce': (   'Washington Eastern District Court',
                  '09/15/1970',
                  '11/09/2011'),
    'wawbke': (   'Washington Western Bankruptcy Court',
                  '01/02/1986',
                  '11/09/2011'),
    'wawdce': (   'Washington Western District Court',
                  '04/02/1971',
                  '11/09/2011'),
    'wiebke': (   'Wisconsin Eastern Bankruptcy Court',
                  '01/02/1990',
                  '11/10/2011'),
    'wiedce': ('Wisconsin Eastern District Court', '04/13/1965', '11/10/2011'),
    'wiwbke': (   'Wisconsin Western Bankruptcy Court',
                  '01/02/1991',
                  '11/10/2011'),
    'wiwdce': ('Wisconsin Western District Court', '04/29/1971', '11/10/2011'),
    'wvnbke': (   'West Virginia Northern Bankruptcy Court',
                  '01/02/1991',
                  '11/10/2011'),
    'wvndce': (   'West Virginia Northern District Court',
                  '05/20/1968',
                  '11/10/2011'),
    'wvsbke': (   'West Virginia Southern Bankruptcy Court',
                  '01/02/1986',
                  '11/10/2011'),
    'wvsdce': (   'West Virginia Southern District Court',
                  '10/17/1956',
                  '11/10/2011'),
    'wybke': ('Wyoming Bankruptcy Court', '01/02/1992', '11/09/2011'),
    'wydce': ('Wyoming District Court', '05/30/1971', '11/09/2011')}
