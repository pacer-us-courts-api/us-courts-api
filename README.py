'''
PACER US Courts Application Programming interface
Copyright (C) 2012 Michael E. Sander (speedplane)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

NOTE: Certain libraries are used by this software, namely BeautifulSoup.
That file is not covered by this license.
'''


print """
------------------------------------------------
-      Quick and Dirty HOWTO by Example        -
------------------------------------------------
"""
import pprint
from docket import docket


pp = pprint.PrettyPrinter(indent=4)

# Create a docket downloader object
dd = docket.DocketDownloader()

print "Logging into PACER."
# Put your username and password here
dd.login(user="username", password="password", matter="")

print "Searching for the Docket 2011-00784"
results, pages, cost = dd.search("2011-00784")
print "Printing Results"
pp.pprint(results)
print "Printing Cost"
pp.pprint(cost)

# Pick the first search result
r = results[0]


def link_processor(href, filing_number, exhibit_num):
	'''
	The contents of a docket entry often contains links to other docket items. 
	This function lets you to process those links into a different form.
	
	Input: See the parameters above, hopefully they are self-explanatory.
	Output: A url for the the link which will be replace the links in the
	contents of the docket.
	'''
	fix = lambda x: urllib2.quote(str(x).replace(" ", "_").replace(":", "--"))
	href = "www.example.com/cases/2011-00784"
	if filing_number:
		href += "%s/"%(str(filing_number))
		if exhibit_num:
			href += "%s/"%(str(exhibit_num))
	return href


# Now get the docket for the search result
out = dd.getdocket(r['docket'], r['link'], r['title'], link_processor)

docket_report = out['docket_report']
pp.pprint(docket_report)
'''
out['docket_report'] is a list of dictionary items with all sorts of information. 
	date 		Date of the docket entry
	pacer_link	A link to download the docketed item on PACER, if it exists
	link		A link that has been processed through link_processor
	contents	The text associated with the summary of the docket entry
	number		The docket number, if it exists
	exhibits	A list of attached exhibits, dictionaries, with the following entries
					exhibit		the exhibit name or number
					pacer_link 	a link to the exhibit on pacer
					link		an internal link to the exhibit
'''

del out['docket_report']
# The docket report also contains a bunch of other case information
# It should hopefully be self-expanatory when you view the output.
# Note that the docket may not contain this information, so you must 
# always check for None
pp.pprint(out)

print "Test Downloading a filing PDF"
# Download the first document in the docket
docket_entry = docket_report[0]
data, cost = dd.getdocument(docket_entry['pacer_link'])
f = open("tmp.pdf", "wb")
f.write(data)
f.close()
		
		
